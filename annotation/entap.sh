#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=100G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load anaconda/4.4.0
module load perl/5.28.1
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/EnTAP --paths /labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/entap_config.txt --runP -i /labs/Wegrzyn/Pellies_Melis/transdecoder/inc_combine.fasta.transdecoder.pep --out-dir /labs/Wegrzyn/Pellies_Melis/annotation_inc -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --contam bacteria --contam fungi --contam insecta  --threads 30
/labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/EnTAP --paths /labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/entap_config.txt --runP -i /labs/Wegrzyn/Pellies_Melis/transdecoder/obl_combine.fasta.transdecoder.pep --out-dir /labs/Wegrzyn/Pellies_Melis/annotation_obl -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --contam bacteria --contam fungi --contam insecta  --threads 30
/labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/EnTAP --paths /labs/Wegrzyn/EnTAP/EnTAP_v0.9.1/EnTAP/entap_config.txt --runP -i /labs/Wegrzyn/Pellies_Melis/transdecoder/au_combine.fasta.transdecoder.pep --out-dir /labs/Wegrzyn/Pellies_Melis/annotation_au -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --contam bacteria --contam fungi --contam insecta  --threads 30
