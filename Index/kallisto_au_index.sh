#!/bin/bash
#SBATCH --job-name=kallisto_index_au
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load kallisto/0.44.0

kallisto index -i au_index ../clustering/au_cds_no_contam.fasta 
