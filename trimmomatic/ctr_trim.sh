
#!/bin/bash
#SBATCH --job-name=trimmomatic
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general

cd /labs/Wegrzyn/Pellies_Melis/merged_reads

module load java
module load Trimmomatic/0.36

ADAPT=/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq2-PE.fa

java -jar $Trimmomatic  PE \
ctr_au_F.fastq.gz ctr_au_R.fastq.gz \
Ctr_au_F_trim.fastq.gz Ctr_au_F_untrim.fastq.gz \
Ctr_au_R_trim.fastq.gz Ctr_au_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_bub_F.fastq.gz Ctr_bub_R.fastq.gz \
Ctr_bub_F_trim.fastq.gz Ctr_bub_F_untrim.fastq.gz \
Ctr_bub_R_trim.fastq.gz Ctr_bub_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_inc_F.fastq.gz Ctr_inc_R.fastq.gz \
Ctr_inc_F_trim.fastq.gz Ctr_inc_F_untrim.fastq.gz \
Ctr_inc_R_trim.fastq.gz Ctr_inc_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_obl_F.fastq.gz Ctr_obl_R.fastq.gz \
Ctr_obl_F_trim.fastq.gz Ctr_obl_F_untrim.fastq.gz \
Ctr_obl_R_trim.fastq.gz Ctr_obl_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_au_10_F.fastq.gz Ctr_au_10_R.fastq.gz \
Ctr_au_10_F_trim.fastq.gz Ctr_au_10_F_untrim.fastq.gz \
Ctr_au_10_R_trim.fastq.gz Ctr_au_10_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_bub_10_F.fastq.gz Ctr_bub_10_R.fastq.gz \
Ctr_bub_10_F_trim.fastq.gz Ctr_bub_10_F_untrim.fastq.gz \
Ctr_bub_10_R_trim.fastq.gz Ctr_bub_10_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_inc_10_F.fastq.gz Ctr_inc_10_R.fastq.gz \
Ctr_inc_10_F_trim.fastq.gz Ctr_inc_10_F_untrim.fastq.gz \
Ctr_inc_10_R_trim.fastq.gz Ctr_inc_10_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45

java -jar $Trimmomatic  PE \
Ctr_obl_10_F.fastq.gz Ctr_obl_10_R.fastq.gz \
Ctr_obl_10_F_trim.fastq.gz Ctr_obl_10_F_untrim.fastq.gz \
Ctr_obl_10_R_trim.fastq.gz Ctr_obl_10_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:25 \
MINLEN:45