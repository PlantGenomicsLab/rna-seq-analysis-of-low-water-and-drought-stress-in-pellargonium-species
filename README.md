## Transcriptome analysis of Pelargonium species under drought and low water stress

## Introduction



### Directory Layout
The data used in this project can be accessed in the Xanadu cluster at the following directory:
`/labs/Wegrzyn/Pellies_Melis`
<br>
To view the files use the following command:
<br>
<pre style="color: silver; background: black;">-bash-4.2$ ls /labs/Wegrzyn/Pellies_Melis
Index extra_files raw_fastqc transdecoder trinity clustering merged_reads raw_reads transdecoder_rnaquast counts new_rnaquast rnaQuast trimmo_4:17  </pre>

The following table have the names given to the samples:
<br>

| Sample | Date harvested | Species | Reads before QC | Reads after QC (q=20/ L=40) | Reads after QC (q=17, L=40) | 
| :------: | :------: |  :------: | :------: |  :------: |------: |  
| Ctr_au | 06-Apr-14 |*Pelargonium auritum*| 12954117 | 3765284 | 8796865 |
| Drought_au | 06-Apr-14 |*Pelargonium auritum*| 27003829 | 8361988 | 19068006 |
| Water_au | 06-Apr-14 |*Pelargonium auritum*| 8147286 | 2405875 | 5664584 |
| Ctr_inc | 06-Apr-14 |*Pelargonium incrassatum*| 19371457 | 5542862 | 13322179 |
| Drought_inc | 06-Apr-14 |*Pelargonium incrassatum*| 13961945 | 3804222 | 9235661 |
| Water_inc | 06-Apr-14 |*Pelargonium incrassatum*| 41259162 | 12578148 | 28792195 |
| Ctr_bub | 06-Apr-14 |*Pelargonium bubonifolium*| 8185776 | 3867647 | 5433767 |
| Drought_bub | 06-Apr-14 |*Pelargonium bubonifolium*| 11286100 | 5763169 | 7904427 |
| Water_bub | 06-Apr-14 |*Pelargonium bubonifolium*| 9837939 | 2681777 | 6431579 |
| Ctr_obl | 06-Apr-14 |*Pelargonium oblongatum*| 8217707 | 2250346 | 5341246 |
| Drought_obl | 06-Apr-14 |*Pelargonium oblongatum*| 14266790 | 4153234 | 9792320 |
| Water_obl | 06-Apr-14 |*Pelargonium oblongatum*| 19235238 | 5445175 | 12635406 |
| Ctr_au_10 | 06-Apr-14 |*Pelargonium auritum*| 7766831 | 2424321 | 5501997 | 5501997 |
| Drought_au_10 | 10-Apr-14 |*Pelargonium auritum*| 11239982 | 3251513 | 7464858 |
| Water_au_10 | 10-Apr-14 |*Pelargonium auritum*| 16102168 | 4471680 | 10633895 |
| Ctr_inc_10 | 10-Apr-14 |*Pelargonium incrassatum*| 19383208 | 5592779 | 12992871 | 
| Drought_inc_10 | 10-Apr-14 |*Pelargonium incrassatum*| 16902696 | 5024673 | 11657509
| Water_inc_10 | 10-Apr-14 |*Pelargonium incrassatum*| 10913426 | 2936023 | 7152201 |
| Ctr_bub_10 | 10-Apr-14 |*Pelargonium bubonifolium*| 9684228 | 2948971 | 6734628 | 
| Drought_bub_10 | 10-Apr-14 |*Pelargonium bubonifolium*| 6863283 | 1797791 | 4416483 |
| Water_bub_10 | 10-Apr-14 |*Pelargonium bubonifolium*| 12111519 | 2867316 | 6987073 |
| Ctr_obl_10 | 10-Apr-14 |*Pelargonium oblongatum*| 16356400 | 3882541 | 11349571 | 
| Drought_obl_10 | 10-Apr-14 |*Pelargonium oblongatum*| 14271401 | 5946224 | 13879888 |
| Water_obl_10 | 10-Apr-14 |*Pelargonium oblongatum*| 11803512 | 3237009 | 8051086 |

<br>

## Quality control Using Sickle 
Sickle was run on raw data. The summary of the data can be seen here: 
Within the `quality_control_sickle` sub-directory, you will find all the trimmed_fastq files of the different samples:
<pre style="color: silver; background: black;">
-bash-4.2$ ls quality_control_sickle/
rem_sickle_run.sh         singles_ctr_bub_10_F.fastq     singles_drought_obl_10_F.fastq  trimmed_ctr_au_F.fastq      trimmed_ctr_obl_F.fastq        trimmed_drought_inc_F.fastq    trimmed_water_bub_F.fastq
sickle_run.sh             singles_ctr_bub_F.fastq        singles_drought_obl_F.fastq     trimmed_ctr_au_R.fastq      trimmed_ctr_obl_R.fastq        trimmed_drought_inc_R.fastq    trimmed_water_bub_R.fastq
sickle_run_752232.out     singles_ctr_inc_10_F.fastq     singles_water_au_10_F.fastq     trimmed_ctr_bub_10_F.fastq  trimmed_drought_au_10_F.fastq  trimmed_drought_obl_10_F.fastq trimmed_water_inc_10_F.fastq
sickle_run_752238.err     singles_ctr_inc_F.fastq        singles_water_au_F.fastq        trimmed_ctr_bub_10_R.fastq  trimmed_drought_au_10_R.fastq  trimmed_drought_obl_10_R.fastq trimmed_water_inc_10_R.fastq
sickle_run_752238.out     singles_ctr_obl_10_F.fastq     singles_water_bub_10_F.fastq    trimmed_ctr_bub_F.fastq     trimmed_drought_au_F.fastq     trimmed_drought_obl_F.fastq    trimmed_water_inc_F.fastq
sickle_run_752238.out     singles_ctr_obl_F.fastq        singles_water_bub_F.fastq       trimmed_ctr_bub_R.fastq     trimmed_drought_au_R.fastq     trimmed_drought_obl_R.fastq    trimmed_water_inc_R.fastq
sickle_run_752287.err     singles_drought_au_10_F.fastq  singles_water_inc_10_F.fastq    trimmed_ctr_inc_10_F.fastq  trimmed_drought_bub_10_F.fastq trimmed_water_au_10_F.fastq    trimmed_water_obl_10_F.fastq
sickle_run_752287.out     singles_drought_au_F.fastq     singles_water_inc_F.fastq       trimmed_ctr_inc_10_R.fastq  trimmed_drought_bub_10_R.fastq trimmed_water_au_10_R.fastq    trimmed_water_obl_10_R.fastq
sickle_run_752315.err     singles_drought_bub_10_F.fastq singles_water_obl_10_F.fastq    trimmed_ctr_inc_F.fastq     trimmed_drought_bub_F.fastq    trimmed_water_au_F.fastq       trimmed_water_obl_F.fastq
sickle_run_752315.out     singles_drought_bub_F.fastq    singles_water_obl_F.fastq       trimmed_ctr_inc_R.fastq     trimmed_drought_bub_R.fastq    trimmed_water_au_R.fastq       trimmed_water_obl_R.fastq 
singles_ctr_au_10_F.fastq singles_drought_inc_10_F.fastq trimmed_ctr_au_10_F.fastq       trimmed_ctr_obl_10_F.fastq  trimmed_drought_inc_10_F.fastq trimmed_water_bub_10_F.fastq
singles_ctr_au_F.fastq    singles_drought_inc_F.fastq    trimmed_ctr_au_10_R.fastq       trimmed_ctr_obl_10_R.fastq  trimmed_drought_inc_10_R.fastq trimmed_water_bub_10_R.fastq </pre>
<br>

The full slurm script is called [sickle_run.sh](/trimmed_reads/sickle_run.sh) can be found in **trimmed_reads/** folder.

<pre style="color: silver; background: black;">
-bash-4.2$ nano sickle_run.sh
#!/bin/bash
#SBATCH --job-name=sickle_run
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH -o sickle_run_%j.out
#SBATCH -e sickle_run_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle

sickle pe -t sanger -f ../raw_reads/Ctr_inc_10/Ctr_inc_10_F.fastq.gz -r ../raw_reads/Ctr_inc_10/Ctr_inc_10_R.fastq.gz -o trimmed_ctr_inc_10_F.fastq -p trimmed_ctr_inc_10_R.fastq -l 45 -q 25 -s singles_ctr_inc_10_F.fastq
</pre>

<pre style="color: silver; background: black;">
-bash-4.2$ sbatch sickle_run.sh
</pre>

To see how the quality of data changed after using sickle, commandline versions of Fastqc and MultiQC were used. 
The full slurm script is called [fastqc.sh](/trimmed_fastqc/fastqc.sh) can be found in **trimmed_fastqc/** folder.

<pre style="color: silver; background: black;">
-bash-4.2$ nano quality_control.sh
#!/bin/bash
#SBATCH --job-name=quality_control
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load fastqc
module load MultiQC/1.7
#Running in
#trimmed_fastqc

fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_au_F.fastq ../trimmed_reads/trimmed_ctr_au_R.fastq

multiqc -n trimmed_fastqc .
</pre>
<br>

## Quality control Using Trimmomatic
Trimmomatic was run on raw data to remove the adapter content. 
Within the `quality_control_trimmomatic` sub-directory, you will find all the sample_trim_fastq files of the different samples:
<pre style="color: silver; background: black;">
-bash-4.2$ ls quality_control_trimmomatic/
Ctr_au_10_F_trim.fastq.gz   Ctr_inc_R_trim.fastq.gz      Water_bub_10_F_trim.fastq.gz  Water_obl_F_trim.fastq.gz      drought_au_F_trim.fastq.gz      drought_inc_R_trim.fastq.gz        trimmomatic_804361_4294967294.out
Ctr_au_10_R_trim.fastq.gz   Ctr_obl_10_F_trim.fastq.gz   Water_bub_10_R_trim.fastq.gz  Water_obl_R_trim.fastq.gz      drought_au_R_trim.fastq.gz      drought_obl_10_F_trim.fastq.gz     trimmomatic_804398_4294967294.err
Ctr_bub_10_F_trim.fastq.gz  Ctr_obl_10_R_trim.fastq.gz   Water_bub_F_trim.fastq.gz     ctr_au_10_trimmo.sh            drought_bub_10_F_trim.fastq.gz  drought_obl_10_R_trim.fastq.gz     trimmomatic_804398_4294967294.out
Ctr_bub_10_R_trim.fastq.gz  Ctr_obl_F_trim.fastq.gz      Water_bub_R_trim.fastq.gz     ctr_au_F_trim.fastq.gz         drought_bub_10_R_trim.fastq.gz  drought_obl_F_trim.fastq.gz        trimmomatic_804432_4294967294.err
Ctr_bub_F_trim.fastq.gz     Ctr_obl_R_trim.fastq.gz      Water_inc_10_F_trim.fastq.gz  ctr_au_R_trim.fastq.gz         drought_bub_F_trim.fastq.gz     drought_obl_R_trim.fastq.gz        trimmomatic_804432_4294967294.out
Ctr_bub_R_trim.fastq.gz     Water_au_10_F_trim.fastq.gz  Water_inc_10_R_trim.fastq.gz  ctr_trim.sh                    drought_bub_R_trim.fastq.gz     drought_trim.sh                    trimmomatic_804924_4294967294.err
Ctr_inc_10_F_trim.fastq.gz  Water_au_10_R_trim.fastq.gz  Water_inc_R_trim.fastq.gz     drought_10_trimmomatic.sh      drought_inc_10_F_trim.fastq.gz  trimmomatic_804351_4294967294.err  trimmomatic_804924_4294967294.out
Ctr_inc_10_R_trim.fastq.gz  Water_au_F_trim.fastq.gz     Water_obl_10_F_trim.fastq.gz  drought_au_10_F_trim.fastq.gz  drought_inc_10_R_trim.fastq.gz  trimmomatic_804351_4294967294.out  water_inc_F_trim.fastq.gz
Ctr_inc_F_trim.fastq.gz     Water_au_R_trim.fastq.gz     Water_obl_10_R_trim.fastq.gz  drought_au_10_R_trim.fastq.gz  drought_inc_F_trim.fastq.gz     trimmomatic_804361_4294967294.err  water_trim.sh </pre>

Trimmomatic was run three times with different parameters:
- slidingwindow-4:25, Minlen-45
- slidingwindow-4:20, Minlen-40
- slidingwindow-4:17, Minlen-40

The adapter file used in the script is TruSeq2-PE.fa. 
The full slurm script is called [ctr.sh](/trimmomatic/ctr.sh) can be found in **trimmomatic/** folder.

<pre style="color: silver; background: black;">
-bash-4.2$ nano ctr_trim.sh
#!/bin/bash
#SBATCH --job-name=trimmomatic
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general

cd /labs/Wegrzyn/Pellies_Melis/merged_reads

module load java
module load Trimmomatic/0.36

ADAPT=/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq2-PE.fa

java -jar $Trimmomatic  PE \
ctr_au_F.fastq.gz ctr_au_R.fastq.gz \
Ctr_au_F_trim.fastq.gz Ctr_au_F_untrim.fastq.gz \
Ctr_au_R_trim.fastq.gz Ctr_au_R_untrim.fastq.gz \
ILLUMINACLIP:$ADAPT:2:30:10 \
LEADING:3 \
TRAILING:3 \
SLIDINGWINDOW:4:20 \
MINLEN:40

</pre>

To see how the quality of data changed after using sickle, commandline versions of Fastqc and MultiQC were used. 
The full slurm script is called [trimmo_quality_control.sh](/trimmomatic_fastqc/trimmo_quality_control.sh) can be found in **trimmomatic_fastqc/** folder.

<pre style="color: silver; background: black;">
-bash-4.2$ nano trimmo_quality_control.sh
#!/bin/bash
#SBATCH --job-name=quality_control
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load fastqc
module load MultiQC/1.7

#Running in
#trimmed_fastqc

fastqc -outdir . -t 4 ../trimmomatic/ctr_au_F_trim.fastq.gz ../trimmomatic/ctr_au_R_trim.fastq.gz

multiqc -n trimmomatic_fastqc .
</pre>

The summary of the data can be seen here: [QC_analysis_sickle_and_trimmomatic.xlsx](/Quality control/QC_analysis_sickle_and_trimmomatic.xlsx) can be found in **Quality control/** folder.

## Kraken2

Kraken is a system for assigning taxonomic labels to short DNA sequences, usually obtained through metagenomic studies. 
Kraken 2 is the newest version of Kraken, a taxonomic classification system using exact k-mer matches to achieve high accuracy and fast classification speeds. 
This classifier matches each k-mer within a query sequence to the lowest common ancestor (LCA) of all genomes containing the given k-mer. 

The script was split into three parts for efficiency -- ctr_kraken.sh, water_kraken.sh, drought_kraken.sh. 
The full slurm script is called [water_kraken.sh](/kraken/water_kraken.sh) can be found in **kraken/** folder.

<pre style="color: silver; background: black;">
-bash-4.2$ nano water_kraken.sh

#!/bin/bash
#SBATCH --job-name=kraken2
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load kraken/2.0.8-beta
#kraken/1.0
module load jellyfish/1.1.11

kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_au_kraken2_report.out --gzip-compressed --paired --unclassified-out water_au#.fastq water_au_1.fastq water_au_2.fastq --threads 3 ../trimmomatic/Water_au_F_trim.fastq.gz ../trimmomatic/Water_au_R_trim.fastq.gz > Water_au_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_inc_kraken2_report.out --gzip-compressed --paired --unclassified-out water_inc#.fastq water_inc_1.fastq water_inc_2.fastq --threads 3 ../trimmomatic/Water_inc_F_trim.fastq.gz ../trimmomatic/Water_inc_R_trim.fastq.gz > Water_inc_kraken.out

</pre>
Within the `kraken` sub-directory, you will find all the unclassified sample_#.fastq files of the different samples:

<pre style="color: silver; background: black;">
-bash-4.2$ ls kraken/
Ctr_au_10_kraken.out            Water_bub_10_kraken.out          ctr_au_10_1.fastq   ctr_obl_10_1.fastq                drought_bub_10_2.fastq             drought_inc_kraken2_report.out     kraken2_925377.err    water_bub_10_2.fastq
Ctr_au_kraken.out               Water_bub_10_kraken2_report.out  ctr_au_10_2.fastq   ctr_obl_10_2.fastq                drought_bub_10_kraken.out          drought_kraken.sh                  kraken2_925377.out    water_bub_2.fastq
Ctr_au_kraken2_report.out       Water_bub_kraken.out             ctr_au_2.fastq      ctr_obl_2.fastq                   drought_bub_10_kraken2_report.out  drought_obl_1.fastq                kraken2_925401.err    water_inc_1.fastq
Ctr_bub_10_kraken.out           Water_bub_kraken2_report.out     ctr_bub_1.fastq     drought_au_1.fastq                drought_bub_2.fastq                drought_obl_10_1.fastq             kraken2_925401.out    water_inc_10_1.fastq
Ctr_bub_kraken.out              Water_inc_10_kraken.out          ctr_bub_10_1.fastq  drought_au_10_1.fastq             drought_bub_kraken.out             drought_obl_10_2.fastq             outfiles              water_inc_10_2.fastq
Ctr_inc_10_kraken.out           Water_inc_10_kraken2_report.out  ctr_bub_10_2.fastq  drought_au_10_2.fastq             drought_bub_kraken2_report.out     drought_obl_10_kraken.out          test.sh               water_inc_2.fastq
Ctr_inc_kraken.out              Water_inc_kraken.out             ctr_bub_2.fastq     drought_au_10_kraken.out          drought_inc_1.fastq                drought_obl_10_kraken2_report.out  unctr.sh              water_kraken.sh
Ctr_obl_10_kraken.out           Water_inc_kraken2_report.out     ctr_inc_1.fastq     drought_au_10_kraken2_report.out  drought_inc_10_1.fastq             drought_obl_2.fastq                water_au_1.fastq      water_obl_1.fastq
Ctr_obl_kraken.out              Water_obl_10_kraken.out          ctr_inc_10_1.fastq  drought_au_2.fastq                drought_inc_10_2.fastq             drought_obl_kraken.out             water_au_10_1.fastq   water_obl_10_1.fastq
Water_au_10_kraken.out          Water_obl_10_kraken2_report.out  ctr_inc_10_2.fastq  drought_au_kraken.out             drought_inc_10_kraken.out          drought_obl_kraken2_report.out     water_au_10_2.fastq   water_obl_10_2.fastq
Water_au_10_kraken2_report.out  Water_obl_kraken.out             ctr_inc_2.fastq     drought_au_kraken2_report.out     drought_inc_10_kraken2_report.out  kraken.sh                          water_au_2.fastq      water_obl_2.fastq
Water_au_kraken.out             Water_obl_kraken2_report.out     ctr_kraken.sh       drought_bub_1.fastq               drought_inc_2.fastq                kraken2_925353.err                 water_bub_1.fastq
Water_au_kraken2_report.out     ctr_au_1.fastq                   ctr_obl_1.fastq     drought_bub_10_1.fastq            drought_inc_kraken.out             kraken2_925353.out                 water_bub_10_1.fastq

</pre>
## De novo assembling the transcriptome using Trinity

Because the genomes are not given, we had to construct them de novo using a transcript assembly software called Trinity. Several scripts were used to run this software due to the immense RAM and time required.
Several scripts were used to run this software due to the immense RAM and time required; four libraries were run per job. In the `trinity` sub-directory
you will find three shell scripts: [trinity.sh](/trinity/trinity.sh), and [trinity2.sh](/trinity/trinity2.sh) can be found in **trinity/** folder. Here is a preview of what `trinity.sh` looks like:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=trinity
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=100G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load samtools/1.9
module load trinity/2.8.5

Trinity --seqType fq \
        --left ../trimmo_4:17/kraken/ctr_bub_1.fastq \
        --right ../trimmo_4:17/kraken/ctr_bub_2.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_ctr_bub \
        --full_cleanup

Trinity --seqType fq \
        --left ../trimmo_4:17/kraken/drought_bub_1.fastq \
        --right ../trimmo_4:17/kraken/drought_bub_2.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_drought_bub \
        --full_cleanup

Trinity --seqType fq \
        --left ../trimmo_4:17/kraken/water_bub_1.fastq \
        --right ../trimmo_4:17/kraken/water_bub_2.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_water_bub \
        --full_cleanup </pre>
        
Following the Trinity runs, the resulting file structure in the `trinity` sub-directory was as follows:
<pre style="color: silver; background: black;">-bash-4.2$ ls 
trinity.sh          trinity_929437.out                               trinity_ctr_obl.Trinity.fasta                       trinity_drought_bub_10.Trinity.fasta.gene_trans_map  trinity_water_bub.Trinity.fasta.gene_trans_map
trinity2.sh         trinity_929444.err                               trinity_ctr_obl.Trinity.fasta.gene_trans_map        trinity_drought_obl.Trinity.fasta                    trinity_water_bub_10.Trinity.fasta
trinity_925498.err  trinity_929444.out                               trinity_ctr_obl_10.Trinity.fasta                    trinity_drought_obl.Trinity.fasta.gene_trans_map     trinity_water_bub_10.Trinity.fasta.gene_trans_map
trinity_925498.out  trinity_ctr_au.Trinity.fasta                     trinity_ctr_obl_10.Trinity.fasta.gene_trans_map     trinity_drought_obl_10.Trinity.fasta                 trinity_water_obl.Trinity.fasta
trinity_925499.err  trinity_ctr_au.Trinity.fasta.gene_trans_map      trinity_drought_au.Trinity.fasta                    trinity_drought_obl_10.Trinity.fasta.gene_trans_map  trinity_water_obl.Trinity.fasta.gene_trans_map
trinity_925499.out  trinity_ctr_au_10.Trinity.fasta                  trinity_drought_au.Trinity.fasta.gene_trans_map     trinity_water_au.Trinity.fasta                       trinity_water_obl_10.Trinity.fasta
trinity_929261.err  trinity_ctr_au_10.Trinity.fasta.gene_trans_map   trinity_drought_au_10.Trinity.fasta                 trinity_water_au.Trinity.fasta.gene_trans_map        trinity_water_obl_10.Trinity.fasta.gene_trans_map
trinity_929261.out  trinity_ctr_bub.Trinity.fasta                    trinity_drought_au_10.Trinity.fasta.gene_trans_map  trinity_water_au_10
trinity_929262.err  trinity_ctr_bub.Trinity.fasta.gene_trans_map     trinity_drought_bub.Trinity.fasta                   trinity_water_au_10.Trinity.fasta
trinity_929262.out  trinity_ctr_bub_10.Trinity.fasta                 trinity_drought_bub.Trinity.fasta.gene_trans_map    trinity_water_au_10.Trinity.fasta.gene_trans_map
trinity_929437.err  trinity_ctr_bub_10.Trinity.fasta.gene_trans_map  trinity_drought_bub_10.Trinity.fasta                trinity_water_bub.Trinity.fasta
</pre>

## Evaluating Trinity assemblies: rnaQUAST
To summarize the assembled transcripts, rnaQUAST was run on each Trinity output.
The script, [rnaQuast.sh](/rnaQuast/rnaQuast.sh) found in the `rnaQuast` sub-directory looks like this: 

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=rnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "rnaQUAST"


module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1


rnaQUAST.py --transcripts ../trinity/trinity_ctr_au.Trinity.fasta ../trinity/trinity_ctr_au_10.Trinity.fasta ../trinity/trinity_drought_au.Trinity.fasta ../trinity/trinity_drought_au_10.Trinity.fasta ../trinity/trinity_water_au.Trinity.f$
        --gene_mark \
        --threads 8 \
        --output_dir results
        </pre>
        
Following the run, the resulting file structure in the `rnaQUAST` sub-directory was as follows:
<pre style="color: silver; background: black;">-bash-4.2$ ls         
results  rnaQuast.sh  rnaQuast_937606.err  rnaQuast_937606.out
</pre>

The results can be viewed under [short_report.pdf](/rnaQuast/short_report.pdf) and [short_report.txt](/rnaQuast/short_report.txt) in the **rnaQuast/folder**.
<br>

|             RNAQuast - auritum                                                   	|ctr	|ctr_10	|water	|water_10|drought |drought_10|
|---------------------------------------------------------------------|-------|--------|------|----|-------|------|
|Transcripts                                                          |	42129	|34317|	35529|	44901|	59635|	40382|
|Transcripts > 500 bp                                                     |	31848|	24968|	25889|	34189|	46039|	30289|
|Transcripts > 1000 bp                                                    |	19652|	14078|	14539|	21122|	30201|	18104|
|Average length of assembled transcripts                         	|1187.415 |1022.419	|1039.648	|1173.202	|1284.882	|1123.662|
|Longest transcript                                                   |	18219|	18171|	17214	|11596	|17017|	17339|
|Total length                                                        	|50024624|	35086338|	36937637|	52677950|	76623930|	45375717|
|Transcript N50                                            |	3186|	1455	|922|	377|	2068	|1246|
<br>

|             RNAQuast - oblongatum                                                   	|ctr	|ctr_10	|water	|water_10|drought |drought_10|
|---------------------------------------------------------------------|-------|--------|------|----|-------|------|
|Transcripts                                                          |	35322	|36382|	38969|	38445|	38604|	54953|
|Transcripts > 500 bp                                                     |	24702|	26791|	28999|	27621|	28174|	41570|
|Transcripts > 1000 bp                                                    |	13168|	14000|	15001|	14193|	14748|	24331|
|Average length of assembled transcripts                         	|1006.529 |975.806	|983.225	|958.645	|970.838	|1084.231|
|Longest transcript                                                   |	23669|	21095|	24076	|19536	|22338|	19004|
|Total length                                                        	|35552601|	35501778|	38315295|	36855110|	37478244|	59581768|
|Transcript N50                                            |	2801|	2515	|1086|	2321|	1813	|3843|
<br>

|             RNAQuast - incrassatum                                                   	|ctr	|ctr_10	|water	|water_10|drought |drought_10|
|---------------------------------------------------------------------|-------|--------|------|----|-------|------|
|Transcripts                                                          |	41848	|45216|	58428|	38079|	38716|	51644|
|Transcripts > 500 bp                                                     |	31309|	34623|	44625|	27531|	28666|	39565|
|Transcripts > 1000 bp                                                    |	17513|	21346|	28173|	14614|	16208|	25065|
|Average length of assembled transcripts                         	|1027.891 |1175.399	|1185.73	|1001.498	|1036.918	|1193.637|
|Longest transcript                                                   |	23669|	21095|	24076	|19536	|22338|	19004|
|Total length                                                        	|43015165|	53146849|	69279844|	38136038|	40145313|	61644180|
|Transcript N50                                            |	1981|	1145	|1076|	1330|	714	|1204|
<br>


## Identifying coding regions using TransDecoder

Transdecoder is used to identify likely coding regions within transcript sequences. 
It identifies long open reading frames (ORFs) within transcripts and scores them according to their sequence composition.

Before submitting any scripts, unique headers were created per each Trinity sample in the `trinity` directory:
<pre style="color: silver; background: black;">
sed 's/>/>au_/g' trinity_ctr_au.Trinity.fasta > ctr_au_prefix.fasta
sed 's/>/>au_/g' trinity_drought_au.Trinity.fasta > drought_au_prefix.fasta
sed 's/>/>au_/g' trinity_water_au.Trinity.fasta > water_au_prefix.fasta
sed 's/>/>au_/g' trinity_ctr_inc.Trinity.fasta > ctr_inc_prefix.fasta
sed 's/>/>au_/g' trinity_ctr_inc_10.Trinity.fasta > ctr_inc_10_prefix.fasta
sed 's/>/>au_/g' trinity_drought_inc.Trinity.fasta > drought_inc_prefix.fasta
sed 's/>/>au_/g' trinity_drought_inc_10.Trinity.fasta > drought_inc_10_prefix.fasta
sed 's/>/>au_/g' trinity_water_inc.Trinity.fasta > water_inc_prefix.fasta
sed 's/>/>au_/g' trinity_ctr_obl_10.Trinity.fasta > ctr_obl_10_prefix.fasta
sed 's/>/>au_/g' trinity_drought_obl_10.Trinity.fasta > drought_obl_10_prefix.fasta
sed 's/>/>au_/g' trinity_drought_obl.Trinity.fasta > drought_obl_prefix.fasta
sed 's/>/>au_/g' trinity_water_obl.Trinity.fasta > water_obl_prefix.fasta </pre>

Next, we can concatenate these assemblies into single file:

<pre style="color: silver; background: black;">
cat ctr_au_prefix.fasta drought_au_prefix.fasta water_au_prefix.fasta >> au_combine.fasta
cat ctr_inc_prefix.fasta ctr_inc_10_prefix.fasta drought_inc_prefix.fasta drought_inc_10_prefix.fasta water_inc_prefix.fasta >> inc_combine.fasta
cat ctr_obl_10_prefix.fasta drought_obl_10_prefix.fasta drought_obl_prefix.fasta water_obl_prefix.fasta >> obl_combine.fasta
</pre>

Now that we have our reads assembled and combined together into single file, we can use TransDecoder to determine optimal open reading frames from the assembly (ORFs).

Within the 'transdecoder' sub-directory scripts can be found to run this job. The scripts were split into 3 different parts: au, obl, inc.
The script, [transdecoder_au.sh](/transdecoder/transdecoder_au.sh) found in the `transdecoder` sub-directory looks like this:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=transdecoder_au
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 10
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder_au"

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../trinity/au_combine.fasta

hmmscan --cpu 10 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm au_combine.fasta.transdecoder_dir/longest_orfs.pep


TransDecoder.Predict -t ../trinity/au_combine.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout
</pre>

## Clustering with VSEARCH

In the sub-directory 'clustering' you will find the scripts: vsearch_au.sh, vsearch_inc.sh, vsearch_obl.sh.
The script for vsearch_au.sh looks like
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=vsearch_au
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast ../Coding_Regions/au_combine.fasta.transdecoder.cds \
        --id 0.90 \
        --centroids centroids_au.fasta \
        --uc clusters_au.uc </pre>
        
At the end of the run it will produce the following files:
<pre style="color: silver; background: black;">
-bash-4.2$ ls 
centroids_au.fasta
clusters_au.uc </pre>

## Identifying duplicate FASTA files: .pep vs .cds

PEP transdecoder FASTA files were compared to the original CDS files. A series of UNIX commands and seqtk was used to execute this task:

<pre style="color: silver; background: black;">
$ srun --partition=general --qos=general --mem=5G --pty bash   
$ module load seqtk/1.2
</pre>

**au:**
<pre style="color: silver; background: black;">

$ grep -h ">" centroids_au.fasta > centroid_au_headers.txt
$ grep -h ">" centroids_au_pep.fasta > centroid_au_pep_headers.txt
$ cat centroid_au_headers.txt centroid_au_pep_headers.txt > au_combine.txt
$ sed -i 's/^.//' au_combine.txt
$ sort au_combine.txt > au_sorted_combine.txt
$ uniq -d au_sorted_combine.txt > au_duplicates.txt
$ seqtk subseq centroids_au.fasta au_duplicates.txt > au_cds.fasta </pre>

The output files from seqtk were used for Kallisto.


|RNAQuast  |au_cds.fasta| obl_cds.fasta| inc_cds.fasta
|-|-------------------------------------------|-----|-----|
|Transcripts	|28001| 28909| 32041|
|Transcripts > 500 bp|	18592| 17840 | 20227 |
|Transcripts > 1000 bp	|9716| 7619 | 10171 |
|Average length of assembled transcripts|	927.166| 776.07 | 886.792 |
|Longest transcript|	7272| 7263 | 7263 |
|Total length|	25961562| 22435395 | 27836967 |
|Transcript N50|	1794| 1242 | 762 |


## Annotation

EnTAP was run against the transcripts from Transdecoder with three databases (1) complete.protein.faa.87.dmnd (2) plant.protein.faa.97.dmnd (3) uniprot_sprot.dmnd. The full slurm script is called [entap.sh](/Entap_new/entap.sh) can be found in **Entap_new/** folder. The script for auritum species looks like this:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

hostname
date
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.36
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.4/EnTAP/EnTAP --ini /labs/Wegrzyn/Pellies_Melis/Entap_new/entap_config.ini --runP -i /labs/Wegrzyn/Pellies_Melis/clustering/au_cds.fasta --out-dir /labs/Wegrzyn/Pellies_Melis/Entap_new/annotation_au_new -d /labs/Wegrzyn/Pellies_Melis/Entap_new/complete.protein.faa.87.dmnd  -d /labs/Wegrzyn/Pellies_Melis/Entap_new/plant.protein.faa.97.dmnd -d /labs/Wegrzyn/Pellies_Melis/Entap_new/uniprot_sprot.dmnd -t 8 
</pre>

The output of this run generates three directories (1) annotation_au_new (2) annotation_inc_new (3) annotation_obl_new. The log file for each species can be found in **Entap_new/** folder as [annotation_au.txt](Entao_new/annotation_au.txt) for a particular species.  

 
## Removing contaminants before indexing

<pre style="color: silver; background: black;">
$ srun --partition=general --qos=general --mem=5G --pty bash   
$ module load seqtk/1.2
</pre>

**au:**
<pre style="color: silver; background: black;">

cd Entap_new/annotation_au_new/final_results/
$ seqtk subseq final_annotated.faa au_annotated_headers_no_contam.txt > au_annotated_no_contam.faa
cd clustering
$ seqtk subseq au_cds.fasta au_annotated_headers_no_contam.txt > au_cds_no_contam.fasta 

</pre>
|species |before removing contaminants (cds.fasta)| after removing contaminants (no_contam.fasta)|
|-|-------------------------------------------|-----|
|au| 28001 | 24448|
|inc| 32041 | 27146|
|obl| 28909 | 24794|

These no_contam.fasta files were used for indexing.   

## Indexing with kallisto
In this step we will be working in the index directory. Before aligning the reads we need to create a index for the created transcoder assembly. 
We will be using Kallisto to build the index using the following command:
The script, [kallisto_au_index.sh](/Index/kallisto_au_index.sh) found in the **Index/folder** looks like this:

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kallisto_index_au
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
##SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load kallisto/0.44.0

kallisto index -i au_index ../clustering/au_cds_no_contam.fasta </pre>

This will create a kallisto index of the centroids.fasta FASTA file, where it will create the following file:

<pre style="color: silver; background: black;">
-bash-4.2$ ls 
au_index
</pre>

## Extraction of Reads Counts using Kallisto
Following the indexing the read counts were extracted using 'kallisto quant' command. The script for kallisto_counts_au.sh looks like this:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=kallisto_counts_au
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load kallisto/0.44.0

kallisto quant -i ../Index/au_index \
        -o au \
        -t 8 --pseudobam \
        -b 50 \
        ../trimmo_4:17/kraken/ctr_au_1.fastq ../trimmo_4:17/kraken/ctr_au_2.fastq

kallisto quant -i ../Index/au_index \
        -o au \
        -t 8 --pseudobam \
        -b 50 \
        ../trimmo_4:17/kraken/water_au_10_1.fastq ../trimmo_4:17/kraken/water_au_10_2.fastq

kallisto quant -i ../Index/au_index \
        -o au \
        -t 8 --pseudobam \
        -b 50 \
        ../trimmo_4:17/kraken/drought_au_1.fastq ../trimmo_4:17/kraken/drought_au_2.fastq

</pre>

The quantification algorithm will produce three output files: 
 *  abundance.h5  : HDF5 binary file   
	This contains run information, abundance estimates, bootstrap estimates and transcript lenght information
 *  abundance.tsv : plaintext file of the abundance estimates  
	This contains effective length, estimated counts and TPM values  
 *  run_info.json : information on the run  
  

## Mapping with Samtools
Using the pseudoalignments.bam file from kallisto, mapping rate were found.
<pre style="color: silver; background: black;">
$ srun --partition=general --qos=general --mem=5G --pty bash 
$ module load samtools/1.7 </pre>
Samtools flagstat was used in the `Mapping ` directory on all the samples for each species.
<br>

<pre style="color: silver; background: black;">
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/au_ctr/pseudoalignments.bam >> au_ctr.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/au_drought/pseudoalignments.bam >> au_drought.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/au_water_10/pseudoalignments.bam >> au_water_10.txt 

$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/inc_ctr/pseudoalignments.bam >> inc_ctr.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/inc_ctr_10/pseudoalignments.bam >> inc_ctr_10.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/inc_drought/pseudoalignments.bam >> inc_drought.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/inc_drought_10/pseudoalignments.bam >> inc_drought_10.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/inc_water/pseudoalignments.bam >> inc_water.txt

$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/obl_water/pseudoalignments.bam >> obl_water.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/obl_drought/pseudoalignments.bam >> obl_drought.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/obl_drought_10/pseudoalignments.bam >> obl_drought_10.txt
$ samtools flagstat /labs/Wegrzyn/Pellies_Melis/counts/obl_ctr_10/pseudoalignments.bam >> obl_ctr_10.txt

</pre>

|Sample Names	|Percentage mapped| Reads mapped|
|--------|--------|-----------|
|au_ctr|	69.05%| 12381551|
|au_drought|	67.91%| 26428276|
|au_water_10|	68.38%| 14667571|
|inc_ctr|	62.12%| 16863392|
|inc_ctr_10|	68.71%| 18539591|
|inc_drought|	63.4%| 11862276|
|inc_drought_10|66.1%| 15744433|
|inc_water|	65.15%| 38982395|
|obl_ctr_10|	54.38%| 12308520|
|obl_drought|	58.96%| 11799321|
|obl_drought_10|57.21%| 15871147|
|obl_water|	56.87%| 14281800|

## Differential expression analysis using DESeq2

<pre style="color: silver; background: black;">
**Incrassatum:**
cd /labs/Wegrzyn/Pellies_Melis/trinity
sed 's/TRINITY/ctr_inc_TRINITY/g' trinity_ctr_inc.Trinity.fasta.gene_trans_map > ctr_inc_map.fasta 
sed 's/TRINITY/ctr_inc_10_TRINITY/g' trinity_ctr_inc_10.Trinity.fasta.gene_trans_map > ctr_inc_10_map.fasta
sed 's/TRINITY/drought_inc_TRINITY/g' trinity_drought_inc.Trinity.fasta.gene_trans_map > drought_inc_map.fasta
sed 's/TRINITY/drought_inc_10_TRINITY/g' trinity_drought_inc_10.Trinity.fasta.gene_trans_map > drought_inc_10_map.fasta 
cat ctr_inc_map.fasta ctr_inc_10_map.fasta drought_inc_map.fasta drought_inc_10_map.fasta  >> inc_map
awk '{ print $2" " $1}' inc_map | sed '1 i\TXNAME\tGENEID' | sed 's/ /\t/g' > tx2gene_inc.tsv 
</pre>

This is the workflow of importing the .counts files for incrassatum:
```R 
library("DESeq2")
directory <- ("~/Downloads/incrassatum")
setwd(directory)
list.files(directory)
outputPrefix <- ("inc_DESeq2")
sampleFiles <- c("inc_ctr_10.counts","inc_ctr.counts","inc_drought_10.counts","inc_drought.counts")
sampleNames <- c("inc_ctr_10","inc_ctr","inc_drought_10","inc_drought")
sampleCondition <- c("control","control","drought","drought")
sampleTable <- data.frame(sampleName = sampleNames, fileName = sampleFiles, condition = sampleCondition)

library(dplyr)
list.files()
count_dir <- getwd()
csv_out <- getwd()
image_out <- getwd()
m = data.frame()

for (i in list.files(pattern = ".counts")) {
  print(paste0("reading file: ", i))
  #read file as a data frame
  
  f <- read.table(i, sep = "\t", header = TRUE)
  #rename the columns
  colnames(f) <- c("gene_id", substr(i, 1, nchar(i)-7))
  #copy the data to another dataframe called f1
  f1 <- subset(f, select= c("gene_id", substr(i, 1, nchar(i)-7)))
  
  #if the m is empty just copy the f to m
  if(length(m) == 0){
    m = f1
  } else 
  {
    #if the dataframe is not empty then merge the data
    m <- merge(m, f1, by.x = "gene_id", by.y = "gene_id")
  }
  rm(f1)
}


rownames(m) <- m[,1]
m <- select(m, "inc_ctr_10", "inc_ctr", "inc_drought_10", "inc_drought")
rm(f)
View(m)
Sample = c("inc_ctr_10", "inc_ctr", "inc_drought_10", "inc_drought")
Condition = factor(c("control", "control", "drought", "drought"))
myfactors <- data.frame(Sample, Condition)
myfactors

# Rounded counts to integers as kallisto provided fractions
dds <- DESeqDataSetFromMatrix(as.matrix(round(m, 0)), DataFrame(Condition), ~Condition)
dds <- DESeq(dds)

res <- results(dds)
transf <- res$padj
transfpad <- -log10(transf)
CFvolData <- data.frame("l2fc" = res$log2FoldChange, "log10padj" = transfpad)
CFvolData <- na.omit(CFvolData)
write.table(CFvolData, file = "CFvolcanodata.csv", sep = ",")
summary(res)
res= subset(res, padj<0.1)
res <- res[order(res$padj),]
resdata <- merge(as.data.frame(res),
                 as.data.frame(counts(dds,normalized =TRUE)),
                 by = 'row.names', sort = FALSE)
names(resdata)[1] <- 'gene'
write.csv(resdata, file = paste0(outputPrefix, "-results-with-normalized.csv"))
write.table(as.data.frame(counts(dds),normalized=T), 
            file = paste0(outputPrefix, "_normalized_counts.txt"), sep = '\t')
pos <- subset(res, log2FoldChange > 2)
neg <- subset(res, log2FoldChange < -2)
upreg2fold <- subset(pos, log2FoldChange < 4)
upreg4fold <- subset(pos, log2FoldChange > 4)
downreg2fold <- subset(neg, log2FoldChange > -4)
downreg4fold <- subset(neg, log2FoldChange < -4)
res2up <- merge(as.data.frame(upreg2fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res2up)[1] <- 'gene'
write.csv(res2up, file = paste0(outputPrefix, "-upreg2fold.csv"))
res4up <- merge(as.data.frame(upreg4fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res4up)[1] <- 'gene'
write.csv(res4up, file = paste0(outputPrefix, "-upreg4fold.csv"))
res2down <- merge(as.data.frame(downreg2fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res2down)[1] <- 'gene'
write.csv(res2down, file = paste0(outputPrefix, "-downreg2fold.csv"))
res4down <- merge(as.data.frame(downreg4fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res4down)[1] <- 'gene'
write.csv(res4down, file = paste0(outputPrefix, "-downreg4fold.csv"))
plotMA(dds, ylim=c(-8,8),main = "RNAseq obl")
dev.copy(png, paste0(outputPrefix, "-MAplot_initial_analysis.png"))
dev.off()

alpha <- 0.1
cols <- densCols(res$log2FoldChange, -log10(res$pvalue))
plot(res$log2FoldChange, -log10(res$padj), col=cols, panel.first=grid(),
     main="Volcano plot", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)

abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")
gn.selected <- abs(res$log2FoldChange) > 2.5 & res$padj < alpha 
text(res$log2FoldChange[gn.selected],
     -log10(res$padj)[gn.selected],
     lab=rownames(res)[gn.selected ], cex=0.4)
rld <- rlogTransformation(dds, blind=T)
#vsd <- varianceStabilizingTransformation(dds, blind=T)
write.csv(as.data.frame(assay(rld)),file = paste0(outputPrefix, "-rlog-transformed-counts.txt"))
#write.csv(as.data.frame(assay(vsd)),file = paste0(outputPrefix, "-vst-transformed-counts.txt"))


#ddsClean <- replaceOutliersWithTrimmedMean(dds)
#ddsClean <- DESeq(ddsClean)
#tab <- table(initial = results(dds)$padj < 0.1,
#             cleaned = results(ddsClean)$padj < 0.1)
#addmargins(tab)
#write.csv(as.data.frame(tab),file = paste0(outputPrefix, "-replaceoutliers.csv"))
#resClean <- results(ddsClean)
#resClean = subset(res, padj<0.1)
#resClean <- resClean[order(resClean$padj),]
#write.csv(as.data.frame(resClean),file = paste0(outputPrefix, "-replaceoutliers-results.csv"))

library("genefilter")
library("ggplot2")
library("grDevices")
rv <- rowVars(assay(rld))
select <- order(rv, decreasing=T)[seq_len(min(500,length(rv)))]
pc <- prcomp(t(assay(rld)[select,]))
condition <- Condition
scores <- data.frame(pc$x, condition)
(pcaplot <- ggplot(scores, aes(x = PC1, y = PC2, col = (factor(condition))))
  + geom_point(size = 5)
  + ggtitle("Principal Components incrassatum")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c("bottom"),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)))
ggsave(pcaplot,file=paste0(outputPrefix, "-ggplot2.png"))
head(assay(rld))
plot(log2(1+counts(dds,normalized=T)[,1:2]),col='black',pch=20,cex=0.3, main='Log2 transformed')
plot(assay(rld)[,1:2],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")
plot(assay(rld)[,3:4],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")


library("RColorBrewer")
library("gplots")
select <- order(rowMeans(counts(dds,normalized=T)),decreasing=T)[1:30]
my_palette <- colorRampPalette(c("blue",'white','red'))(n=30)
heatmap(assay(rld)[select,], col=my_palette,
        scale="row", key=T, keysize=1, symkey=T,
        density.info="none", trace="none",
        cexCol=0.6, labRow=F,
        main="incrassatum")
dev.copy(png, paste0(outputPrefix, "-distanceHEATMAP.png"))
dev.off()
group <- factor(rep(1:9,each=6))
condition <- factor(rep(rep(c("N","C","S"),each=2),3))
d <- DataFrame(group, condition)[-c(17,18),]
as.data.frame(d)
m1 <- model.matrix(~ condition*group, d)
colnames(m1)
unname(m1)
sampleDists <- dist(t(assay(rld)))
suppressMessages(library("RColorBrewer"))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colnames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colors <- colorRampPalette( rev(brewer.pal(8, "Blues")) )(255)
heatmap(sampleDistMatrix,col=colors,margin = c(8,8))
dev.copy(png,paste0(outputPrefix, "-clustering.png"))
dev.off() 
```

## Differentially Expressed Genes using Gfold ##
Gfold is used when there is only single replicate present per condition. We will use the kallisto generated abundance.tsv file and reformat it, so it will contain the fields which Gfold program needs as its input.   
<pre style="color: silver; background: black;">
awk '{print $1 "\t" $1 "\t" $4 "\t" $2 "\t" $5 }' ../counts/au_water_10/abundance.tsv > au_water_10.read_cnt
awk '{print $1 "\t" $1 "\t" $4 "\t" $2 "\t" $5 }' ../counts/au_drought/abundance.tsv > au_drought.read_cnt </pre>

Now since we have the counts files in the correct format, we will run the Gfold using the following command. We will be using the au_water_10.read_cnt and au_drought.read_cnt as input files.
<pre style="color: silver; background: black;">
module load gfold/1.1.4
gfold diff -s1 au_water_10 -s2 au_drought -suf .read_cnt -o au_water_10_vs_au_drought.diff </pre>

The script [gfold.sh](/Gfold/gfold.sh) found in the **Gfold/folder**. The output directory looks like: 
<pre style="color: silver; background: black;">
au_drought.read_cnt  au_water_10.read_cnt  au_water_10_vs_au_drought.diff  au_water_10_vs_au_drought.diff.ext </pre>


## Orthogroup analysis using OrthoFinder

OrthoFinder was run against each individual age group/species’ FAA file from EnTAP. The script for the orthofinder can be found in  [orthofinder.sh](/orthofinder_revised/orthofinder.sh) found in the **orthofinder_revised/folder** looks like this:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G
#SBATCH -o orthofinder.out
#SBATCH -e orthofinder.err

hostname
date

module load OrthoFinder/2.3.3
module load muscle
module load DLCpar/1.0
module load FastME
module load diamond/0.9.25
module load mcl
#module load anaconda/4.4.0

orthofinder -f /labs/Wegrzyn/Pellies_Melis/orthofinder_revised  -S diamond -t 10 
</pre>

The output directory looks like this :
<pre style="color: silver; background: black;">
OrthoFinder au_annotated_no_contam.faa inc_annotated_no_contam.faa obl_annotated_no_contam.faa orthofinder.err orthofinder.out orthofinder.sh </pre>











