#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G
#SBATCH -o orthofinder.out
#SBATCH -e orthofinder.err

hostname
date

module load OrthoFinder/2.3.3
module load muscle
module load DLCpar/1.0
module load FastME
module load diamond/0.9.25
module load mcl
#module load anaconda/4.4.0

orthofinder -f /labs/Wegrzyn/Pellies_Melis/orthofinder_revised  -S diamond -t 10 
