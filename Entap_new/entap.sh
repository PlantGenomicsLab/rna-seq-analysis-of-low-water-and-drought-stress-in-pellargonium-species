#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

hostname
date
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.36
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.4/EnTAP/EnTAP --ini /labs/Wegrzyn/Pellies_Melis/Entap_new/entap_config.ini --runP -i /labs/Wegrzyn/Pellies_Melis/clustering/au_cds.fasta --out-dir /labs/Wegrzyn/Pellies_Melis/Entap_new/annotation_au -d /labs/Wegrzyn/Pellies_Melis/Entap_new/complete.protein.faa.87.dmnd  -d /labs/Wegrzyn/Pellies_Melis/Entap_new/plant.protein.faa.97.dmnd -d /labs/Wegrzyn/Pellies_Melis/Entap_new/uniprot_sprot.dmnd -t 8 
/labs/Wegrzyn/EnTAP/EnTAP_v0.10.4/EnTAP/EnTAP --ini /labs/Wegrzyn/Pellies_Melis/Entap_new/entap_config.ini --runP -i /labs/Wegrzyn/Pellies_Melis/clustering/obl_cds.fasta --out-dir /labs/Wegrzyn/Pellies_Melis/Entap_new/annotation_obl -d /labs/Wegrzyn/Pellies_Melis/Entap_new/complete.protein.faa.87.dmnd  -d /labs/Wegrzyn/Pellies_Melis/Entap_new/plant.protein.faa.97.dmnd -d /labs/Wegrzyn/Pellies_Melis/Entap_new/uniprot_sprot.dmnd -t 8
/labs/Wegrzyn/EnTAP/EnTAP_v0.10.4/EnTAP/EnTAP --ini /labs/Wegrzyn/Pellies_Melis/Entap_new/entap_config.ini --runP -i /labs/Wegrzyn/Pellies_Melis/clustering/inc_cds.fasta --out-dir /labs/Wegrzyn/Pellies_Melis/Entap_new/annotation_inc -d /labs/Wegrzyn/Pellies_Melis/Entap_new/complete.protein.faa.87.dmnd  -d /labs/Wegrzyn/Pellies_Melis/Entap_new/plant.protein.faa.97.dmnd -d /labs/Wegrzyn/Pellies_Melis/Entap_new/uniprot_sprot.dmnd -t 8
