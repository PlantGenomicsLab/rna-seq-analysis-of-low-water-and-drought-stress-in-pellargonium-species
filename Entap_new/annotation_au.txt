------------------------------------------------------
Final Annotation Statistics
------------------------------------------------------
Total Sequences: 28001. 
Similarity Search
        Total unique sequences with an alignment: 17242
        Total unique sequences without an alignment: 10759
Gene Families
        Total unique sequences with family assignment: 23469
        Total unique sequences without family assignment: 4532
        Total unique sequences with at least one GO term: 19965
        Total unique sequences with at least one pathway (KEGG) assignment: 6003
Totals
        Total unique sequences annotated (similarity search alignments only): 69
        Total unique sequences annotated (gene family assignment only): 6296
        Total unique sequences annotated (gene family and/or similarity search): 23538
        Total unique sequences unannotated (gene family and/or similarity search): 4463
