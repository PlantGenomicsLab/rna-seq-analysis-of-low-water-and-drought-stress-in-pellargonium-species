## Meeting notes 03/08/2021

- [x] Network analysis using cytoscape. Subnetworks formation. Final model how each species is coping up with drought is almost finished.
- [x] gene family analysis in all three species. There are some genes which have more copies in one species than the other.
- [ ] changing the name of transcripts? all three species should have different initial like auritum as Pau, incrassatum as Pinc, and oblongatum as Pobl.





## Meeting notes 




- [x] Annotation with arabidopsis database
- [x] Orthofinder run on the full transcript and DE genes 
- [x] DESEq2 on incrassatum and oblongatum    

- Issues running DESEq2
1. Heat map in sleuth
2. Volcano plot (how to adjust p value in script)

- [ ] Data visualization (cytoscape, g:profiler, REVIGO)


## Meeting notes 01/06/2021

1. Error in Orthofinder 
ERROR: au_annotated_no_contam.faa appears to contain nucleotide sequences instead of amino acid sequences.  

2. Input Entap file au_annotated_no_contam.faa  
>ctr_au_TRINITY_DN3866_c0_g1_i3.p1.   
vstnwghnhgngmrtsrhtadcyssvsvvskmvtdvghgkghngvvtksansvdcyaggkrhsvanngmaavkrsvrtwaytgtkasvam. 

3. Kallisto run ?


## Errors while running EnTap 12/20/2020
1. **On whole transcriptome**  
Script used: entap.sh 
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=100G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

hostname
date
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/EnTAP --runP --ini /labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/entap_config.ini -i /labs/Wegrzyn/Pellies_Melis/clustering/inc_cds.fasta --out-dir /labs/Wegrzyn/Pellies_Melis/annotation_inc -d /labs/Wegrzyn/Pellies_Melis/Entap_new/complete.protein.faa.87.dmnd  -d /labs/Wegrzyn/Pellies_Melis/Entap_new/plant.protein.faa.97.dmnd -d /labs/Wegrzyn/Pellies_Melis/Entap_new/uniprot_sprot.dmnd -t 30
</pre>

**Error** : Error code: 72    
Unable to open EnTAP database from paths given
EnTAP Database Error: Serialized EnTAP database does not exist at: /bin/entap_database.bin

2. **Diamond configuration**    
database used: complete.protein.faa.87.dmnd   
script: test.sh
<pre style="color: silver; background: black;">
/labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/EnTAP --config --ini /labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/entap_config.ini -i /labs/Wegrzyn/Pellies_Melis/clustering/inc_cds.fasta --out-dir /labs/Wegrzyn/Pellies_Melis/annotation_inc -d /labs/Wegrzyn/Pellies_Melis/Entap_new/complete.protein.faa.87.dmnd
</pre>

**Error** : Error code: 10       
Cannot input DIAMOND (.dmnd) database when configuring!

3. **Diamond configuration**    
Database used: swiss_prot_test.fasta
<pre style="color: silver; background: black;">
/labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/EnTAP --config --ini /labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/entap_config.ini --out-dir /labs/Wegrzyn/Pellies_Melis/annotation_inc -d /labs/Wegrzyn/Pellies_Melis/Entap_new/swiss_prot_test.fasta
</pre>

**Error** : Error code: 10    
EggNOG DIAMOND database was not found at: /bin/eggnog_proteins.dmnd
The DIAMOND test run failed.


----------------------------------------------------

## Pelargonium Meeting july 23
- [x] running Entap with protea repens
- [x] graphical representation with orthogroups shared among species ( both all and differentially expressed) 
- [x] searching drought database and literature for already reported drought resistance genes
- [x] comparison between ATannotated pelargonium (auritum, incrassatum, oblongatum) with ATannotated protea repens 
- [x] genemania and g:profiler run with DE genes 
- [ ] orthofinder run with protea transcriptome (will run once entap is done)


## Pelargonium Meeting july 10  

## Orthofinder results

Number of species	3  
Number of genes 404266  
Number of genes in orthogroups  278812  
Number of unassigned genes	125454  
Percentage of genes in orthogroups	69.0  
Percentage of unassigned genes  31.0  
Number of orthogroups   51725  
Number of species-specific orthogroups  132  
Number of genes in species-specific orthogroups 579  
Percentage of genes in species-specific orthogroups     0.1  
Mean orthogroup size    5.4  
Median orthogroup size  4.0  
G50 (assigned genes)    7  
G50 (all genes) 4  
O50 (assigned genes)    11815  
O50 (all genes) 23109  
Number of orthogroups with all species present  25370  
Number of single-copy orthogroups	2927  
 
Orthogroups file        Orthogroups.tsv  
Unassigned genes file   Orthogroups_UnassignedGenes.tsv  
Per-species statistics  Statistics_PerSpecies.tsv  
Overall statistics	Statistics_Overall.tsv  
Orthogroups shared between species	Orthogroups_SpeciesOverlaps.tsv  

Average number of genes per-species in orthogroup	Number of orthogroups   Percentage of orthogroups	Number of genes Percentage of ge$  
<1	14378   27.8    28756   10.3  
'1	20020   38.7    75685   27.1  
'2	8572    16.6    58563   21.0  
'3	4262    8.2     41995   15.1  
'4	2160    4.2     27752   10.0  
'5	1096    2.1     17330   6.2  
'6	553     1.1     10429   3.7  
'7	305     0.6     6658    2.4  
'8	141     0.3     3506    1.3  
'9	83	0.2     2309    0.8  
'10     50	0.1     1546    0.6  
11-15   83	0.2     3067    1.1  
16-20   19	0.0     1007    0.4  
21-50   3	0.0     209     0.1  
51-100  0	0.0     0	0.0  
101-150 0	0.0     0	0.0  
151-200 0	0.0     0	0.0  
201-500 0	0.0     0	0.0  
501-1000        0	0.0     0	0.0  
'1001+  0	0.0     0	0.0  

Number of species in orthogroup Number of orthogroups  
1	132  
2	26223  
3	25370  




## Pelargonium Meeting 19 June

## NOISeq 

For length.txt file in incrassatum- ctr_inc, oblongatum- ctr_obl_10

| Species| Before filter |  |After filter | |
| ------ | ------ | ------ |------ | ------ |
|   | Down reg| UP reg |Down reg| UP reg |
| Obl| 15326 | 2434 | 3549| 690 |
|Inc| 13015 | 3970 | 4374 | 1783 |

Filter = >1.5, < -1.5

## DESeq2
| Species| 2FC |  |4FC | |
| ------ | ------ | ------ |------ |------ |
|   | Down reg| UP reg |Down reg| UP reg |
| Obl| 83 | 221 |5 | 71 |
|Inc| 385 | 490 |36 | 130 |

Transcripts from incrassatum (normalised.csv) = 167  
Transcripts from oblongatum (normalised.csv) = 554

## Gfold in auritum 
Total DE genes = 28001  
both up and down regulated = 184 transcripts (with filter less than -1.5 and greater than 1.5)  
Python script for extracting sequences in fasta format

```
import sys
import csv
from operator import itemgetter


diffFile = sys.argv[1] # .diff file generated by GFOLD
fastaFile = sys.argv[2] # fasta file containning GeneIDs and sequence
n = int(sys.argv[3]) # number of gene IDs to be extracted
'''
diffFile = "/labs/Wegrzyn/Pellies_Melis/gfold/au_water_10_vs_au_drought.diff"
fastaFile = "au_combine.fasta.transdecoder.cds"
n = 3
'''
'''
load diffFile as a table
'''
with open(diffFile) as diff:
    reader = csv.reader(diff, delimiter="\t")
    geneList = list(reader)
geneList = geneList[11:len(geneList)] ##exclude header

'''
sort genes based on GFOLD value
'''
for row in range(0, len(geneList)):
    geneList[row][2] = abs(float(geneList[row][2])) ##convert GFOLD value to float

geneList = sorted(geneList, key=itemgetter(2), reverse=True) ##sort genes descending order

geneIDs = []
for i in range(n):
    geneIDs.append(geneList[0 + i][0].split(":")[0]) ##store first n gene IDs into a list
#print geneIDs
'''
load fasta file
'''
cds = open(fastaFile, 'r')
lines = cds.readlines()
oneLine = '\t'.join([line.strip() for line in lines])
sequence = oneLine.split(">") ##seperate file into a lists individual sequence

'''
search and extract seqences
'''
sqList = []

output = open("ExtractedSq2.fasta", 'w') ##create a output fasta file
 geneList[row][2] = abs(float(geneList[row][2])) ##convert GFOLD value to float

geneList = sorted(geneList, key=itemgetter(2), reverse=True) ##sort genes descending order

geneIDs = []
for i in range(n):
    geneIDs.append(geneList[0 + i][0].split(":")[0]) ##store first n gene IDs into a list
    
#print geneIDs
'''
load fasta file
'''
cds = open(fastaFile, 'r')
lines = cds.readlines()
oneLine = '\t'.join([line.strip() for line in lines])
sequence = oneLine.split(">") ##seperate file into a lists individual sequence

'''
search and extract seqences
'''
sqList = []

output = open("ExtractedSq2.fasta", 'w') ##create a output fasta file

for item in sequence:
    for id in geneIDs:
        if item.startswith(id):
            sq = item.split('\t', 1)[1]
            sq = sq.replace('\t', '') ##reformat sequence to one line
            geneIDs.remove(id)
            sqList.append(sq)
            output.write(">" + id+ "\n" + sq + "\n" + "\n") ##write sequnces in fasta format
        else:
            pass

print "Number of Sequences extracted:", len(sqList)
```

## EnTap reasults after gfold on auritum species with 100 genes

------------------------------------------------------
Final Annotation Statistics
------------------------------------------------------
Total Sequences: 100 

- Similarity Search  
        Total unique sequences with an alignment: 71  
        Total unique sequences without an alignment: 29  

- Gene Families  
	Total unique sequences with family assignment: 84  
        Total unique sequences without family assignment: 16  
        Total unique sequences with at least one GO term: 73  
        Total unique sequences with at least one pathway (KEGG) assignment: 30  

- Totals  
      	Total unique sequences annotated (similarity search alignments only): 1  
        Total unique sequences annotated (gene family assignment only): 14  
        Total unique sequences annotated (gene family and/or similarity search): 85  
        Total unique sequences unannotated (gene family and/or similarity search): 15  

Mapping of Differentially expressing genes on biological processes

| GO | # |
| ------ | ------ |
| response to other organism| 10 |
| regulation of biosynthetic process | 9 |
| regulation of primary metabolic process | 9 |
| organic cyclic compound biosynthetic process | 8 |
| regulation of macromolecule metabolic process | 8 | 
| regulation of cellular metabolic process | 8 |
| response to metal ion | 7 |
| regulation of nitrogen compound metabolic process | 7 |
| ion transport | 7 |
| transmembrane transport | 7 |

## Analysis in Progress 

- [x] Entap (oblongatum and incrassatum) with transcripts extracted from normalised.csv files.  
- [ ] Differential expression analysis on all species   



## Genomics Meeting 17 June 2020
- [x] DESeq2 using kallisto - incrassatum, oblongatum
- [x] DESeq2 with counts files from NOISEq- incrassatum, oblongatum
- [x] NOISEq - incrassatum, oblongatum
- [x] Gfold - auritum
- [x] EnTap - auritum
- [ ] EnTap - incrassatum, oblongatum



## 4 June 2020

- [x] Gfold on auritum samples
- [x] NOISeq on incrassatum samples, generate .counts files and used those files in DESeq2 analysis
- [x] EnTap on all samples 
- [ ] DESeq2 on oblongatum 
- [ ] DESeq2 on among all species

- For DESEq2 analysis i used DDSeqFromMatrix and rounded all the counts values as it was not accepting the original values

## 29 May 2020

1. EnTap- /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd
/isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd
/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd

2. DESeq2 - Incrassatum- ctr(2), drought(2),  Oblongatum- ctr(1), water(1), drought(2)

3. Error in analysis
```R 
par(mai = ifelse(1:4 <= 2, par('mai'),0))
px <- counts(dds)[,1] / sizeFactors(dds)[1]
ord <- order(px)
ord <- ord[px[ord] < 150]
ord <- ord[seq(1,length(ord),length=50)]
last <- ord[length(ord)]
vstcol <- c('blue','black')
matplot(px[ord], cbind(assay(vsd)[,1], log2(px))[ord, ],type='l', lty = 1, col=vstcol, xlab = 'n', ylab = 'f(n)')
```
Error 
```
Error in plot.window(...) : need finite 'xlim' values
In addition: Warning messages:
1: In min(x) : no non-missing arguments to min; returning Inf
2: In max(x) : no non-missing arguments to max; returning -Inf
3: In min(x) : no non-missing arguments to min; returning Inf
4: In max(x) : no non-missing arguments to max; returning -Inf
```

4. GFOLD with auritum - water_au_10, drought_au
5. DESeq2 with all samples

| control | drought |
| ------ | ------ |
| Water_au | drought_au |
| ctr_inc | drought_inc |
| water_inc | drought_inc_10 |
| ctr_obl_10 | drought_obl_10 |
| water_obl | drought_obl |

Error 
```R 
outputPrefix <- "all_deseq2"
setwd("~/Downloads/pel_all_tsv")
filenames <- list.files(pattern = "*abundance.tsv", full.names = TRUE)
filenames
tx2gene <- read.table("tx2gene_all.tsv", header = TRUE)
head(tx2gene)
txi.kallisto <- tximport(filenames, type = "kallisto", tx2gene = tx2gene, ignoreTxVersion = TRUE)

1 2 Error in tximport(filenames, type = "kallisto", tx2gene = tx2gene, ignoreTxVersion = TRUE) : 
  all(txId == raw[[txIdCol]]) is not TRUE
In addition: Warning message:
In txId == raw[[txIdCol]] :
  longer object length is not a multiple of shorter object length
```


## 22 May 2020

1. EnTap- running

2. Error in running sleuth

"s2c <- dplyr::mutate(s2c, path=kallisto_dirs)
> print(s2c)
  sample   condition                                              path
1      1     auritum  ~/Downloads/pelargonium_analysis/au_abundance.h5
2      2 incrassatum ~/Downloads/pelargonium_analysis/inc_abundance.h5
3      3  oblongatum ~/Downloads/pelargonium_analysis/obl_abundance.h5
> so <- sleuth_prep(s2c, ~condition, bootstrap_summary=TRUE, read_bootsrap_tpm=TRUE, transformation_function = function(x) log2(x+0.5))
reading in kallisto results
dropping unused factor levels
...
Error in check_kal_pack(kal_list) : 
  Inconsistent number of transcripts. Please make sure you used the same index everywhere."
i was thinking or running index again i guess that might correct this error. 

3. running DeSeq2 in R. till now its working fine.
