#!/bin/bash
#SBATCH --job-name=trinity
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=100G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load samtools/1.9
module load trinity/2.8.5

Trinity --seqType fq \
        --left ../trimmo_4:17/kraken/ctr_bub_1.fastq \
        --right ../trimmo_4:17/kraken/ctr_bub_2.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_ctr_bub \
        --full_cleanup

Trinity --seqType fq \
        --left ../trimmo_4:17/kraken/drought_bub_1.fastq \
        --right ../trimmo_4:17/kraken/drought_bub_2.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_drought_bub \
        --full_cleanup

Trinity --seqType fq \
        --left ../trimmo_4:17/kraken/water_bub_1.fastq \
        --right ../trimmo_4:17/kraken/water_bub_2.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_water_bub \
        --full_cleanup


