#!/bin/bash
#SBATCH --job-name=quality_control
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general


module load fastqc
module load MultiQC/1.7

#Running in
#trimmed_fastqc

fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_au_F.fastq ../trimmed_reads/trimmed_ctr_au_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_inc_F.fastq ../trimmed_reads/trimmed_ctr_inc_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_obl_F.fastq ../trimmed_reads/trimmed_ctr_obl_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_bub_F.fastq ../trimmed_reads/trimmed_ctr_bub_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_au_10_F.fastq ../trimmed_reads/trimmed_ctr_au_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_inc_10_F.fastq ../trimmed_reads/trimmed_ctr_inc_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_obl_10_F.fastq ../trimmed_reads/trimmed_ctr_obl_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_ctr_bub_10_F.fastq ../trimmed_reads/trimmed_ctr_bub_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_au_F.fastq ../trimmed_reads/trimmed_drought_au_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_inc_F.fastq ../trimmed_reads/trimmed_drought_inc_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_obl_F.fastq ../trimmed_reads/trimmed_drought_obl_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_bub_F.fastq ../trimmed_reads/trimmed_drought_bub_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_au_10_F.fastq ../trimmed_reads/trimmed_drought_au_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_inc_10_F.fastq ../trimmed_reads/trimmed_drought_inc_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_obl_10_F.fastq ../trimmed_reads/trimmed_drought_obl_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_drought_bub_10_F.fastq ../trimmed_reads/trimmed_drought_bub_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_au_F.fastq ../trimmed_reads/trimmed_water_au_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_inc_F.fastq ../trimmed_reads/trimmed_water_inc_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_obl_F.fastq ../trimmed_reads/trimmed_water_obl_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_bub_F.fastq ../trimmed_reads/trimmed_water_bub_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_au_10_F.fastq ../trimmed_reads/trimmed_water_au_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_inc_10_F.fastq ../trimmed_reads/trimmed_water_inc_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_obl_10_F.fastq ../trimmed_reads/trimmed_water_obl_10_R.fastq
fastqc -outdir . -t 4 ../trimmed_reads/trimmed_water_bub_10_F.fastq ../trimmed_reads/trimmed_water_bub_10_R.fastq

multiqc -n trimmed_fastqc .
