#!/bin/bash
#SBATCH --job-name=transdecoder_au
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 10
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder_au"

module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../trinity/au_combine.fasta

hmmscan --cpu 10 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm au_combine.fasta.transdecoder_dir/longest_orfs.pep


TransDecoder.Predict -t ../trinity/au_combine.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout