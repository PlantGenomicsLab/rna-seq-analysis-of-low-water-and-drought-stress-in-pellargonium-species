#!/bin/bash
#SBATCH --job-name=quality_control
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general


module load fastqc
module load MultiQC/1.7

#Running in
#trimmed_fastqc

fastqc -outdir . -t 4 ../trimmomatic/ctr_au_F_trim.fastq.gz ../trimmomatic/ctr_au_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_inc_F_trim.fastq.gz ../trimmomatic/Ctr_inc_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_bub_F_trim.fastq.gz ../trimmomatic/Ctr_bub_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_obl_F_trim.fastq.gz ../trimmomatic/Ctr_obl_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_au_10_F_trim.fastq.gz ../trimmomatic/Ctr_au_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_inc_10_F_trim.fastq.gz ../trimmomatic/Ctr_inc_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_bub_10_F_trim.fastq.gz ../trimmomatic/Ctr_bub_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Ctr_obl_10_F_trim.fastq.gz ../trimmomatic/Ctr_obl_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_au_F_trim.fastq.gz ../trimmomatic/drought_au_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_inc_F_trim.fastq.gz ../trimmomatic/drought_inc_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_bub_F_trim.fastq.gz ../trimmomatic/drought_bub_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_obl_F_trim.fastq.gz ../trimmomatic/drought_obl_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_au_10_F_trim.fastq.gz ../trimmomatic/drought_au_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_inc_10_F_trim.fastq.gz ../trimmomatic/drought_inc_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_bub_10_F_trim.fastq.gz ../trimmomatic/drought_bub_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/drought_obl_10_F_trim.fastq.gz ../trimmomatic/drought_obl_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_au_F_trim.fastq.gz ../trimmomatic/Water_au_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/water_inc_F_trim.fastq.gz ../trimmomatic/Water_inc_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_bub_F_trim.fastq.gz ../trimmomatic/Water_bub_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_obl_F_trim.fastq.gz ../trimmomatic/Water_obl_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_au_10_F_trim.fastq.gz ../trimmomatic/Water_au_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_inc_10_F_trim.fastq.gz ../trimmomatic/Water_inc_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_bub_10_F_trim.fastq.gz ../trimmomatic/Water_bub_10_R_trim.fastq.gz
fastqc -outdir . -t 4 ../trimmomatic/Water_obl_10_F_trim.fastq.gz ../trimmomatic/Water_obl_10_R_trim.fastq.gz

multiqc -n trimmomatic_fastqc .