#!/bin/bash
#SBATCH --job-name=sickle_run
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH -o sickle_run_%j.out
#SBATCH -e sickle_run_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle


sickle pe -t sanger -f ../raw_reads/Ctr_inc_10/Ctr_inc_10_F.fastq.gz -r ../raw_reads/Ctr_inc_10/Ctr_inc_10_R.fastq.gz -o trimmed_ctr_inc_10_F.fastq -p trimmed_ctr_inc_10_R.fastq -l 45 -q 25 -s singles_ctr_inc_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Ctr_au_10/Ctr_au_10_F.fastq.gz -r ../raw_reads/Ctr_au_10/Ctr_au_10_R.fastq.gz -o trimmed_ctr_au_10_F.fastq -p trimmed_ctr_au_10_R.fastq -l 45 -q 25 -s singles_ctr_au_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Ctr_bub_10/Ctr_bub_10_F.fastq.gz -r ../raw_reads/Ctr_bub_10/Ctr_bub_10_R.fastq.gz -o trimmed_ctr_bub_10_F.fastq -p trimmed_ctr_bub_10_R.fastq -l 45 -q 25 -s singles_ctr_bub_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Ctr_obl_10/Ctr_obl_10_F.fastq.gz -r ../raw_reads/Ctr_obl_10/Ctr_obl_10_R.fastq.gz -o trimmed_ctr_obl_10_F.fastq -p trimmed_ctr_obl_10_R.fastq -l 45 -q 25 -s singles_ctr_obl_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_inc_10/Drought_inc_10_F.fastq.gz -r ../raw_reads/Drought_inc_10/Drought_inc_10_R.fastq.gz -o trimmed_drought_inc_10_F.fastq -p trimmed_drought_inc_10_R.fastq -l 45 -q 25 -s singles_drought_inc_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_obl_10/Drought_obl_10_F.fastq.gz -r ../raw_reads/Drought_obl_10/Drought_obl_10_R.fastq.gz -o trimmed_drought_obl_10_F.fastq -p trimmed_drought_obl_10_R.fastq -l 45 -q 25 -s singles_drought_obl_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_bub_10/Drought_bub_10_F.fastq.gz -r ../raw_reads/Drought_bub_10/Drought_bub_10_R.fastq.gz -o trimmed_drought_bub_10_F.fastq -p trimmed_drought_bub_10_R.fastq -l 45 -q 25 -s singles_drought_bub_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_au_10/Drought_au_10_F.fastq.gz -r ../raw_reads/Drought_au_10/Drought_au_10_R.fastq.gz -o trimmed_drought_au_10_F.fastq -p trimmed_drought_au_10_R.fastq -s singles_drought_au_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_au_10/Water_au_10_F.fastq.gz -r ../raw_reads/Water_au_10/Water_au_10_R.fastq.gz -o trimmed_water_au_10_F.fastq -p trimmed_water_au_10_R.fastq -l 45 -q 25 -s singles_water_au_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_inc_10/Water_inc_10_F.fastq.gz -r ../raw_reads/Water_inc_10/Water_inc_10_R.fastq.gz -o trimmed_water_inc_10_F.fastq -p trimmed_water_inc_10_R.fastq -l 45 -q 25 -s singles_water_inc_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_obl_10/Water_obl_10_F.fastq.gz -r ../raw_reads/Water_obl_10/Water_obl_10_R.fastq.gz -o trimmed_water_obl_10_F.fastq -p trimmed_water_obl_10_R.fastq -l 45 -q 25 -s singles_water_obl_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_bub_10/Water_bub_10_F.fastq.gz -r ../raw_reads/Water_bub_10/Water_bub_10_R.fastq.gz -o trimmed_water_bub_10_F.fastq -p trimmed_water_bub_10_R.fastq -l 45 -q 25 -s singles_water_bub_10_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_au/Water_au_F.fastq.gz -r ../raw_reads/Water_au/Water_au_R.fastq.gz -o trimmed_water_au_F.fastq -p trimmed_water_au_R.fastq -l 45 -q 25 -s singles_water_au_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_inc/Water_inc_F.fastq.gz -r ../raw_reads/Water_inc/Water_inc_R.fastq.gz -o trimmed_water_inc_F.fastq -p trimmed_water_inc_R.fastq -l 45 -q 25 -s singles_water_inc_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_bub/Water_bub_F.fastq.gz -r ../raw_reads/Water_bub/Water_bub_R.fastq.gz -o trimmed_water_bub_F.fastq -p trimmed_water_bub_R.fastq -l 45 -q 25 -s singles_water_bub_F.fastq
sickle pe -t sanger -f ../raw_reads/Water_obl/Water_obl_F.fastq.gz -r ../raw_reads/Water_obl/Water_obl_R.fastq.gz -o trimmed_water_obl_F.fastq -p trimmed_water_obl_R.fastq -l 45 -q 25 -s singles_water_obl_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_au/Drought_au_F.fastq.gz -r ../raw_reads/Drought_au/Drought_au_R.fastq.gz -o trimmed_drought_au_F.fastq -p trimmed_drought_au_R.fastq -l 45 -q 25 -s singles_drought_au_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_inc/Drought_inc_F.fastq.gz -r ../raw_reads/Drought_inc/Drought_inc_R.fastq.gz -o trimmed_drought_inc_F.fastq -p trimmed_drought_inc_R.fastq -l 45 -q 25 -s singles_drought_inc_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_bub/Drought_bub_F.fastq.gz -r ../raw_reads/Drought_bub/Drought_bub_R.fastq.gz -o trimmed_drought_bub_F.fastq -p trimmed_drought_bub_R.fastq -l 45 -q 25 -s singles_drought_bub_F.fastq
sickle pe -t sanger -f ../raw_reads/Drought_obl/Drought_obl_F.fastq.gz -r ../raw_reads/Drought_obl/Drought_obl_R.fastq.gz -o trimmed_drought_obl_F.fastq -p trimmed_drought_obl_R.fastq -l 45 -q 25 -s singles_drought_obl_F.fastq
sickle pe -t sanger -f ../raw_reads/ctr_au/ctr_au_F.fastq.gz -r ../raw_reads/ctr_au/ctr_au_R.fastq.gz -o trimmed_ctr_au_F.fastq -p trimmed_ctr_au_R.fastq -l 45 -q 25 -s singles_ctr_au_F.fastq
sickle pe -t sanger -f ../raw_reads/Ctr_inc/Ctr_inc_F.fastq.gz -r ../raw_reads/Ctr_inc/Ctr_inc_R.fastq.gz -o trimmed_ctr_inc_F.fastq -p trimmed_ctr_inc_R.fastq -l 45 -q 25 -s singles_ctr_inc_F.fastq
sickle pe -t sanger -f ../raw_reads/Ctr_bub/Ctr_bub_F.fastq.gz -r ../raw_reads/Ctr_bub/Ctr_bub_R.fastq.gz -o trimmed_ctr_bub_F.fastq -p trimmed_ctr_bub_R.fastq -l 45 -q 25 -s singles_ctr_bub_F.fastq
sickle pe -t sanger -f ../raw_reads/Ctr_obl/Ctr_obl_F.fastq.gz -r ../raw_reads/Ctr_obl/Ctr_obl_R.fastq.gz -o trimmed_ctr_obl_F.fastq -p trimmed_ctr_obl_R.fastq -l 45 -q 25 -s singles_ctr_obl_F.fastq