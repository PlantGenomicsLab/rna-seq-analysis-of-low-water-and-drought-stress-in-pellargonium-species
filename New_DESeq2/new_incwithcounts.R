library("DESeq2")
directory <- ("~/Downloads/incrassatum")
setwd(directory)
list.files(directory)
outputPrefix <- ("inc_DESeq2")
sampleFiles <- c("inc_ctr_10.counts","inc_ctr.counts","inc_drought_10.counts","inc_drought.counts")
sampleNames <- c("inc_ctr_10","inc_ctr","inc_drought_10","inc_drought")
sampleCondition <- c("control","control","drought","drought")
sampleTable <- data.frame(sampleName = sampleNames, fileName = sampleFiles, condition = sampleCondition)

library(dplyr)
list.files()
count_dir <- getwd()
csv_out <- getwd()
image_out <- getwd()
m = data.frame()

for (i in list.files(pattern = ".counts")) {
  print(paste0("reading file: ", i))
  #read file as a data frame
  
  f <- read.table(i, sep = "\t", header = TRUE)
  #rename the columns
  colnames(f) <- c("gene_id", substr(i, 1, nchar(i)-7))
  #copy the data to another dataframe called f1
  f1 <- subset(f, select= c("gene_id", substr(i, 1, nchar(i)-7)))
  
  #if the m is empty just copy the f to m
  if(length(m) == 0){
    m = f1
  } else 
  {
    #if the dataframe is not empty then merge the data
    m <- merge(m, f1, by.x = "gene_id", by.y = "gene_id")
  }
  rm(f1)
}


rownames(m) <- m[,1]
m <- select(m, "inc_ctr_10", "inc_ctr", "inc_drought_10", "inc_drought")
rm(f)
View(m)
Sample = c("inc_ctr_10", "inc_ctr", "inc_drought_10", "inc_drought")
Condition = factor(c("control", "control", "drought", "drought"))
myfactors <- data.frame(Sample, Condition)
myfactors

# Rounded counts to integers as kallisto provided fractions
dds <- DESeqDataSetFromMatrix(as.matrix(round(m, 0)), DataFrame(Condition), ~Condition)
dds <- DESeq(dds)

res <- results(dds)
transf <- res$padj
transfpad <- -log10(transf)
CFvolData <- data.frame("l2fc" = res$log2FoldChange, "log10padj" = transfpad)
CFvolData <- na.omit(CFvolData)
write.table(CFvolData, file = "CFvolcanodata.csv", sep = ",")
summary(res)
res= subset(res, padj<0.1)
res <- res[order(res$padj),]
resdata <- merge(as.data.frame(res),
                 as.data.frame(counts(dds,normalized =TRUE)),
                 by = 'row.names', sort = FALSE)
names(resdata)[1] <- 'gene'
write.csv(resdata, file = paste0(outputPrefix, "-results-with-normalized.csv"))
write.table(as.data.frame(counts(dds),normalized=T), 
            file = paste0(outputPrefix, "_normalized_counts.txt"), sep = '\t')
pos <- subset(res, log2FoldChange > 2)
neg <- subset(res, log2FoldChange < -2)
upreg2fold <- subset(pos, log2FoldChange < 4)
upreg4fold <- subset(pos, log2FoldChange > 4)
downreg2fold <- subset(neg, log2FoldChange > -4)
downreg4fold <- subset(neg, log2FoldChange < -4)
res2up <- merge(as.data.frame(upreg2fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res2up)[1] <- 'gene'
write.csv(res2up, file = paste0(outputPrefix, "-upreg2fold.csv"))
res4up <- merge(as.data.frame(upreg4fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res4up)[1] <- 'gene'
write.csv(res4up, file = paste0(outputPrefix, "-upreg4fold.csv"))
res2down <- merge(as.data.frame(downreg2fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res2down)[1] <- 'gene'
write.csv(res2down, file = paste0(outputPrefix, "-downreg2fold.csv"))
res4down <- merge(as.data.frame(downreg4fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res4down)[1] <- 'gene'
write.csv(res4down, file = paste0(outputPrefix, "-downreg4fold.csv"))
plotMA(dds, ylim=c(-8,8),main = "RNAseq obl")
dev.copy(png, paste0(outputPrefix, "-MAplot_initial_analysis.png"))
dev.off()

alpha <- 0.1
cols <- densCols(res$log2FoldChange, -log10(res$pvalue))
plot(res$log2FoldChange, -log10(res$padj), col=cols, panel.first=grid(),
     main="Volcano plot", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)

abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")
gn.selected <- abs(res$log2FoldChange) > 2.5 & res$padj < alpha 
text(res$log2FoldChange[gn.selected],
     -log10(res$padj)[gn.selected],
     lab=rownames(res)[gn.selected ], cex=0.4)
rld <- rlogTransformation(dds, blind=T)
#vsd <- varianceStabilizingTransformation(dds, blind=T)
write.csv(as.data.frame(assay(rld)),file = paste0(outputPrefix, "-rlog-transformed-counts.txt"))
#write.csv(as.data.frame(assay(vsd)),file = paste0(outputPrefix, "-vst-transformed-counts.txt"))


#ddsClean <- replaceOutliersWithTrimmedMean(dds)
#ddsClean <- DESeq(ddsClean)
#tab <- table(initial = results(dds)$padj < 0.1,
#             cleaned = results(ddsClean)$padj < 0.1)
#addmargins(tab)
#write.csv(as.data.frame(tab),file = paste0(outputPrefix, "-replaceoutliers.csv"))
#resClean <- results(ddsClean)
#resClean = subset(res, padj<0.1)
#resClean <- resClean[order(resClean$padj),]
#write.csv(as.data.frame(resClean),file = paste0(outputPrefix, "-replaceoutliers-results.csv"))

library("genefilter")
library("ggplot2")
library("grDevices")
rv <- rowVars(assay(rld))
select <- order(rv, decreasing=T)[seq_len(min(500,length(rv)))]
pc <- prcomp(t(assay(rld)[select,]))
condition <- Condition
scores <- data.frame(pc$x, condition)
(pcaplot <- ggplot(scores, aes(x = PC1, y = PC2, col = (factor(condition))))
  + geom_point(size = 5)
  + ggtitle("Principal Components incrassatum")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c("bottom"),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)))
ggsave(pcaplot,file=paste0(outputPrefix, "-ggplot2.png"))
head(assay(rld))
plot(log2(1+counts(dds,normalized=T)[,1:2]),col='black',pch=20,cex=0.3, main='Log2 transformed')
plot(assay(rld)[,1:2],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")
plot(assay(rld)[,3:4],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")


library("RColorBrewer")
library("gplots")
select <- order(rowMeans(counts(dds,normalized=T)),decreasing=T)[1:30]
my_palette <- colorRampPalette(c("blue",'white','red'))(n=30)
heatmap(assay(rld)[select,], col=my_palette,
        scale="row", key=T, keysize=1, symkey=T,
        density.info="none", trace="none",
        cexCol=0.6, labRow=F,
        main="incrassatum")
dev.copy(png, paste0(outputPrefix, "-distanceHEATMAP.png"))
dev.off()
group <- factor(rep(1:9,each=6))
condition <- factor(rep(rep(c("N","C","S"),each=2),3))
d <- DataFrame(group, condition)[-c(17,18),]
as.data.frame(d)
m1 <- model.matrix(~ condition*group, d)
colnames(m1)
unname(m1)
sampleDists <- dist(t(assay(rld)))
suppressMessages(library("RColorBrewer"))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colnames(sampleDistMatrix) <- paste(colnames(rld), rld$type, sep="")
colors <- colorRampPalette( rev(brewer.pal(8, "Blues")) )(255)
heatmap(sampleDistMatrix,col=colors,margin = c(8,8))
dev.copy(png,paste0(outputPrefix, "-clustering.png"))
dev.off() 