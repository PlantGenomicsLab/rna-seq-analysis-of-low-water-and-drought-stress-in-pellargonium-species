#!/bin/bash
#SBATCH --job-name=rnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "rnaQUAST"


module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1


rnaQUAST.py --transcripts ../trinity/trinity_ctr_au.Trinity.fasta ../trinity/trinity_ctr_au_10.Trinity.fasta ../trinity/trinity_drought_au.Trinity.fasta ../trinity/trinity_drought_au_10.Trinity.fasta ../trinity/trinity_water_au.Trinity.f$
        --gene_mark \
        --threads 8 \
        --output_dir results

