
#!/bin/bash
#SBATCH --job-name=kraken2
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load kraken/2.0.8-beta
#kraken/1.0
module load jellyfish/1.1.11

kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_au_kraken2_report.out --gzip-compressed --paired --unclassified-out water_au#.fastq water_au_1.fastq water_au_2.fastq --threads 3 ../trimmomatic/Water_au_F_trim.fastq.gz ../trimmomatic/Water_au_R_trim.fastq.gz > Water_au_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_inc_kraken2_report.out --gzip-compressed --paired --unclassified-out water_inc#.fastq water_inc_1.fastq water_inc_2.fastq --threads 3 ../trimmomatic/Water_inc_F_trim.fastq.gz ../trimmomatic/Water_inc_R_trim.fastq.gz > Water_inc_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_obl_kraken2_report.out --gzip-compressed --paired --unclassified-out water_obl#.fastq water_obl_1.fastq water_obl_2.fastq --threads 3 ../trimmomatic/Water_obl_F_trim.fastq.gz ../trimmomatic/Water_obl_R_trim.fastq.gz > Water_obl_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_bub_kraken2_report.out --gzip-compressed --paired --unclassified-out water_bub#.fastq water_bub_1.fastq water_bub_2.fastq --threads 3 ../trimmomatic/Water_bub_F_trim.fastq.gz ../trimmomatic/Water_bub_R_trim.fastq.gz > Water_bub_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_au_10_kraken2_report.out --gzip-compressed --paired --unclassified-out water_au_10#.fastq water_au_10_1.fastq water_au_10_2.fastq --threads 3 ../trimmomatic/Water_au_10_F_trim.fastq.gz ../trimmomatic/Water_au_10_R_trim.fastq.gz > Water_au_10_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_inc_10_kraken2_report.out --gzip-compressed --paired --unclassified-out water_inc_10#.fastq water_inc_10_1.fastq water_inc_10_2.fastq --threads 3 ../trimmomatic/Water_inc_10_F_trim.fastq.gz ../trimmomatic/Water_inc_10_R_trim.fastq.gz > Water_inc_10_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_obl_10_kraken2_report.out --gzip-compressed --paired --unclassified-out water_obl_10#.fastq water_obl_10_1.fastq water_obl_10_2.fastq --threads 3 ../trimmomatic/Water_obl_10_F_trim.fastq.gz ../trimmomatic/Water_obl_10_R_trim.fastq.gz > Water_obl_10_kraken.out
kraken2 --db /isg/shared/databases/kraken2/Minikraken2_v1/ --report Water_bub_10_kraken2_report.out --gzip-compressed --paired --unclassified-out water_bub_10#.fastq water_bub_10_1.fastq water_bub_10_2.fastq --threads 3 ../trimmomatic/Water_bub_10_F_trim.fastq.gz ../trimmomatic/Water_bub_10_R_trim.fastq.gz > Water_bub_10_kraken.out

